var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {

    //合併多個css檔+sass檔，自動合併成 public/css/all.css，原生檔案位於resources/assets/css|sass/
	mix.styles([
	    'materialize.css'
	]);
    mix.sass('app.scss');


    //合併多個js，自動合併成 public/js/all.js，原生檔案位於resources/assets/js/
    mix.scripts([
        'materialize.js'
    ]);
});
