
<!-- jquery -->
<script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>

<!-- googleapis -->
<link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" />

<!-- materialize -->
<link rel="stylesheet" href="{{ asset('/materialize/css/materialize.css') }}" media="screen,projection"/>
<link rel="stylesheet" href="http://fonts.googleapis.com/earlyaccess/notosanstc.css"  media="screen,projection"/>
<script src="{{ asset('materialize/js/materialize.js') }}"></script>

<!-- css -->
<link href="{{ asset('css/base.css') }}" rel="stylesheet" />
<link href="{{ asset('css/section.css') }}" rel="stylesheet" />
<link href="{{ asset('css/unit.css') }}" rel="stylesheet" />
