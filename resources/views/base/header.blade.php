<nav>
    <div class="nav-wrapper blue-grey darken-2">
        <a href="{{ url('/home') }}" class="brand-logo">立捷客戶端</a>
        <ul class="right hide-on-med-and-down">
            <li><a href="{{ url('/home') }}">首頁</a></li>
            <li><a href="{{ url('/logout') }}">登出</a></li>
        </ul>

        @if(!strpos(Request::url(),'home'))
            <a href="{{ URL::previous() }}">
                <ul class="left hide-on-large-only">
                    <i class="large material-icons">reply</i>
                </ul>
            </a>
        @endif

        <!--Mobile DropDown Button-->
        <a class="right dropdown-button hide-on-large-only" href="#!" data-activates="dropdown1">
            <i class="material-icons large">menu</i>
        </a>

        <ul id='dropdown1' class='dropdown-content'>
            <li><a href="{{ url('/home') }}">首頁</a></li>
            <li><a href="{{ url('/logout') }}">登出</a></li>
        </ul>
        <!--Mobile DropDown Button-->
    </div>

</nav>
