@extends('layouts.app')

    @section('content')

    <div class="col s12">
        <a href="http://www.lj-exp.com">
            <div class="card small hoverable" style="height:20%;">
                <div class="card-image">
                    <img src="images/logo.jpg">
                </div>
            </div>
        </a>
    </div>

    @foreach (['A', 'B', 'C', 'D'] as $key => $value)
        <div class="center home-btn">
            <a class="waves-effect waves-light btn-large" href="{{ url('/function'.$value) }}">
                <i class="material-icons left">cloud</i>
                <span>功能{{ $value }}</span>
            </a>
        </div>
    @endforeach

@endsection
