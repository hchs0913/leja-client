<!DOCTYPE html>
<html>
    <head>
        <title>立捷客戶端</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        @include('base.assets')

    </head>

    <body>
        @include('base.header')

        <div class="container">
            @yield('content')
        </div>

    </body>
</html>
