@extends('layouts.app')

@section('content')

	<div class="search-bar">
        <i class="small material-icons">search</i>
		<input type="text" class="search">
	</div>

	<ul class="posts">
	    <li class="post">
	        <dl>
				<dt>編號</dt>
	            <dd>2131</dd>
	        </dl>
	        <div class="post-status">
				<span>未檢查</span>
			</div>
	    </li>
	    <li class="post">
	        <dl>
				<dt>編號</dt>
	            <dd>54654654132132</dd>
	        </dl>
	        <div class="post-status">
				<span>未檢查</span>
			</div>
	    </li>
	    <li class="post">
	        <dl>
				<dt>編號</dt>
	            <dd>4654564</dd>
	        </dl>
	        <div class="post-status">
				<span>未檢查</span>
			</div>
	    </li>
	</ul>

@endsection
