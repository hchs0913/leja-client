@extends('layouts.app')

@section('content')

    <div class="search-bar">
        <i class="small material-icons">search</i>
        <input type="text" class="search">
    </div>

    <div class="post">
        <dl>
            <dt>編號</dt>
            <dd>2131</dd>
        </dl>
        <dl>
            <dt>物流編號</dt>
            <dd>121223131</dd>
        </dl>
        <dl>
            <dt>收件人</dt>
            <dd>鲁伯·海格</dd>
        </dl>
        <dl>
            <dt>寄送地</dt>
            <dd>倫敦國王十字車站九又四分之三月台</dd>
        </dl>
        <dl class="post-status">
            <dt>狀態</dt>
            <dd><span>未檢查</span></dd>
        </dl>
    </div>

@endsection
