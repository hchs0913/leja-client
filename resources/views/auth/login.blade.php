@extends('layouts.html')

@section('content')

<div class="container">
    <div class="row" style="margin-top:50px;">    
        <form class="col s12" role="form" method="POST" action="{{ url('/login') }}">
            {{ csrf_field() }}
            <div class="row">
            <div class="input-field col s12">
                <label for="email">帳號</label>
                <input id="email" type="email" class="validate" name="email" value="">
                    
            </div>
            </div>
            <div class="row">
            <div class="input-field col s12">
                <label for="password">密碼</label>
                <input id="password" type="password" class="validate" name="password">
                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
            </div>
            

            @if ($errors->has('email'))
                        <span class="pink-text text-darken-2">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
            @endif
            <p>
                <input type="checkbox" id="remember" />
                <label for="remember">記住我</label>
            </p>

            
            <div class="row">
                <div class="col s12">
                    <button type="submit" class="waves-effect waves-light btn">
                        <i></i> 登入
                    </button>
                    <!--     停用Forgot Password
                    <a class="btn btn-link" href="{{ url('/password/reset') }}">Forgot Your Password?</a>
                    -->
                </div>
            </div>
        </form>  
    </div>
</div>


@endsection
