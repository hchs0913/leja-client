<?php

use Illuminate\Database\Seeder;
use App\User;
class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'email' => 'root@mynet',
            'name'  => 'mynet',
            'password' => '123456',
        ]);


        foreach(DB::table("users")->get() as $user) {
        DB::table("users")
        ->where("id", $user->id)
        ->update(array("password"=>Hash::make($user->password)));
        }
    }
}
