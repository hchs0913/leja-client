<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        return redirect('/home');
    }
    public function home()
    {
        return view('home');
    }
    public function functionA(){
        return view('pages.functionA');
    }
    public function functionB(){
        return view('pages.functionB');
    }
    public function functionC(){
        return view('pages.functionC');
    }
    public function functionD(){
        return view('pages.functionD');
    }
}
