<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::auth();

Route::get('/', 'HomeController@index');
Route::get('/functionA','HomeController@functionA');
Route::get('/functionB','HomeController@functionB');
Route::get('/functionC','HomeController@functionC');
Route::get('/functionD','HomeController@functionD');
Route::get('/home', 'HomeController@home');